import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from ttkbootstrap import Style
import random

class DemoApp:
    def __init__(self, root):
        self.root = root
        self.root.title("TTKBootstrap Demo")
        # self.root.geometry("400x300")

        # 从ttkbootstrap选择一个随机主题
        theme_list = Style().theme_names()
        self.selected_theme = random.choice(theme_list)
        print(theme_list)
        print(self.selected_theme)
        self.selected_theme='superhero'
        self.style = Style(theme=self.selected_theme)  # 选择一个ttkbootstrap主题

        # 创建和布局窗口部件
        self.create_widgets()

    def create_widgets(self):
        # 标签
        label = ttk.Label(self.root, text="欢迎使用TTKBootstrap演示")
        label.pack(pady=10)

        # 按钮
        button = ttk.Button(self.root, text="点击我！", command=self.show_message)
        button.pack(pady=5)

        # 输入框
        entry = ttk.Entry(self.root)
        entry.pack(pady=5)

        # 复选框
        checkbutton = ttk.Checkbutton(self.root, text="勾选我！")
        checkbutton.pack(pady=5)

        # 单选按钮
        radio_frame = ttk.Frame(self.root)
        radio_frame.pack(pady=5)
        radio_var = tk.StringVar()
        ttk.Radiobutton(radio_frame, text="选项1", variable=radio_var, value="选项1").pack(side="left", padx=5)
        ttk.Radiobutton(radio_frame, text="选项2", variable=radio_var, value="选项2").pack(side="left", padx=5)

        # 下拉框
        combobox = ttk.Combobox(self.root, values=["选项1", "选项2", "选项3"])
        combobox.pack(pady=5)

        # 进度条
        progressbar = ttk.Progressbar(self.root, length=200, mode='indeterminate')
        progressbar.pack(pady=5)
        progressbar.start()       

    def show_message(self):
        messagebox.showinfo("TTKBootstrap演示", "您点击了按钮！")

def main():
    root = tk.Tk()
    app = DemoApp(root)
    root.mainloop()

if __name__ == "__main__":
    main()
