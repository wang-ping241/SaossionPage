from playwright.sync_api import sync_playwright

with sync_playwright() as playwright:
    browser = playwright.chromium.launch(executable_path=r"C:\Program Files\Twinkstar Browser\twinkstar.exe",headless=False)
    context = browser.new_context()
    page = context.new_page()

    # 打开百度网址
    page.goto('https://www.baidu.com')

    # 在搜索框中输入关键词 "Python" 并提交搜索
    page.fill('input[name="wd"]', 'Python')
    page.press('input[name="wd"]', 'Enter')

    # 等待搜索结果加载完成
    page.wait_for_load_state('networkidle')

    # 获取搜索结果页面的标题
    title = page.title()
    print("搜索结果页面标题：", title)

    # 获取搜索结果列表
    search_results = page.query_selector_all('.result')
    for result in search_results:
        # 打印搜索结果标题和链接
        result_title = result.inner_text()
        result_link = result.query_selector('a').get_attribute('href')
        print(f"搜索结果：{result_title} - {result_link}")

    # 关闭浏览器
    browser.close()
