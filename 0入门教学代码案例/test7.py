
from DrissionPage import ChromiumPage,ChromiumOptions

#-配置类
class Config:
    Chrome_path = r"C:\Users\Administrator\AppData\Local\Google\Chrome\Application\chrome.exe"
    UA_android="Mozilla/5.0 (Linux; Android 11; Pixel 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.62 Mobile Safari/537.36"
    UA_apple="Mozilla/5.0 (iPhone; CPU iPhone OS 15_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.0 Mobile/15E148 Safari/604.1"


#-创建配置对象
co=ChromiumOptions()

#-启动配置
co.set_browser_path("C:/Program Files/Twinkstar Browser/twinkstar.exe")
co.set_user_agent(Config.UA_android)
co.mute(True)
co.ignore_certificate_errors(True)


#-创建浏览器
page = ChromiumPage(addr_or_opts=co)

#-打开网址
page.get('https://gitee.com/g1879/DrissionPage')

test=input('go on ?')