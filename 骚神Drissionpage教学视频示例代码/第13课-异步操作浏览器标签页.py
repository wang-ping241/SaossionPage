
#!/usr/bin/env python
# -*- coding:utf-8 -*-# 

# 骚神DP教学
# 电脑内需要提取安装谷歌浏览器或者其他chromium内核的浏览器  比如 edge浏览器  qq浏览器  360浏览器
# Drissionpage官网  http://drissionpage.cn/
# Drissionpage 版本需要大于等于 4.1.0.0

# datarecorder 库使用文档 https://drissionpage.cn/DataRecorderDocs/usage/introduction/

import asyncio
from DrissionPage import Chromium
from DataRecorder import Recorder
from loguru import logger

# 定义异步采集方法
async def collect(tab, recorder, title, page=1):

    # 遍历所有标题元素
    for i in tab.eles('.title project-namespace-path'):
        # 获取某页所有库名称，记录到记录器
        recorder.add_data((title, i.text, page))
        logger.info((title, i.text, page))
        
    # 获取下一页按钮
    btn = tab('@rel=next', timeout=2)
    # 如果有下一页，点击翻页并递归调用自身
    if btn:
        btn.click(by_js=True)
        await asyncio.sleep(1)  # 异步等待
        # 增加页数并递归调用
        await collect(tab, recorder, title, page + 1)

# 主函数
async def main():
    # 新建浏览器对象
    browser = Chromium()

    # 第一个标签页访问网址
    tab1=browser.new_tab('https://gitee.com/explore/ai')
    # 新建一个标签页并访问另一个网址，返回其对象
    tab2 = browser.new_tab('https://gitee.com/explore/machine-learning')

    # 新建记录器对象
    recorder = Recorder('data.csv')

    # 创建异步任务
    task1 = asyncio.create_task(collect(tab1, recorder, 'ai'))
    task2 = asyncio.create_task(collect(tab2, recorder, '机器学习'))

    # 等待任务完成
    await task1
    await task2

    print("所有任务完成")
    recorder.record()

# 运行主函数
if __name__ == '__main__':
    asyncio.run(main())