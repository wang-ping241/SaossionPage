#!/usr/bin/env python
# -*- coding:utf-8 -*-# 

# 骚神DP教学
# 电脑内需要提取安装谷歌浏览器或者其他chromium内核的浏览器  比如 edge浏览器  qq浏览器  360浏览器
# Drissionpage官网  http://drissionpage.cn/
# Drissionpage 版本需要大于等于 4.1.0.0

from DrissionPage import Chromium,ChromiumOptions

# 连接浏览器并获取浏览器对象
co=ChromiumOptions()
browser = Chromium(co)  

# 自动点击确认按钮
tab = browser.new_tab('https://zh.javascript.info/alert-prompt-confirm')

# 确认
tab.eles('t:a@@title=运行')[-2].click()
tab.wait(3)
tab.handle_alert(accept=True)

#取消
tab.eles('t:a@@title=运行')[-2].click()
tab.wait(3)
tab.handle_alert(accept=False)


#提取信息
tab.eles('t:a@@title=运行')[-2].click()
tab.wait(3)
提示框信息=tab.handle_alert(accept=False)
print(提示框信息)

input('Press any key to quit')  
