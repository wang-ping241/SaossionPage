class JS():                       
    listen_mouse_on_ele='''
  
                            
                        function addClickEventToInputs() {
                            // 获取所有输入框元素
                            var inputElements = document.querySelectorAll('a,li,img,input');
                            //var inputElements = document.querySelectorAll('*');

                            // 为每个输入框元素添加点击事件监听器
                            inputElements.forEach(function(inputElement) {
                                inputElement.addEventListener('mouseover', function() {
                                    var id = inputElement.id;
                                    var className = inputElement.className;
                                    var Name = inputElement.tagName;
                                    var text = inputElement.innerText

                                    // 弹出包含 id 和 class 值的提示框
                                    var info="按F8复制元素语法--> tag:"+ Name+"@@id=" + id + "@@class=" + className+"@@text()=" + text
                                    
                                    //alert(info);
                                    document.getElementById('show').textContent = info;
                                });
                            });
                        }

                        // 添加键盘按下事件监听器
                   

                        // 调用函数，为输入框添加点击事件
                        addClickEventToInputs();
                    '''
    
    listen_mouse_on_ele2='''
                        //检查字符串是否为空
                        function isBlankString(str) {
                            return str.trim().length === 0;
                        }
                        // 检查是否包含特殊字符 
                        function containsString(str) {
                            return str.includes('href') || str.includes('src');
                        }

                        //打印某个元素的所有属性值
                        function printElementAttributesAsString(element) {
                            // 检查输入是否是一个元素
                            if (!(element instanceof Element)) {
                                console.error('输入必须是一个HTML元素');
                                return;
                            }

                            // 获取元素的所有属性
                            var attrs = element.attributes;

                            // 初始化一个空字符串用于存储属性
                            var attributesString = '';

                            // 遍历所有属性并将它们的名称和值拼接到字符串中
                            for (var i = 0; i < attrs.length; i++) {
                                var attrName = attrs[i].name;
                                var attrValue = attrs[i].value;
                                //特殊情况处理
                                if (containsString(attrName)) continue;
                                if (isBlankString(attrValue)) continue;

                                if (attrValue.length > 25 && attrName != "class") {
                                    attributesString += "@@" + attrName + "^" + attrValue.slice(0, 20);
                                } else {
                                    // 拼接属性名和属性值，属性之间用空格分隔
                                    attributesString += "@@" + attrName + "=" + attrValue;
                                }



                            }

                            // 打印最终的属性字符串
                            //console.log(attributesString.trim()); // 使用trim()移除尾部的空格
                            return attributesString.trim();
                        }

                        //添加监听
                        function addClickEventToInputs() {
                            // 获取所有输入框元素
                            var inputElements = document.querySelectorAll('a,li,img,input,button');
                            //var inputElements = document.querySelectorAll('*');

                            // 为每个输入框元素添加点击事件监听器
                            inputElements.forEach(function (inputElement) {
                                inputElement.addEventListener('mouseover', function () {


                                    var attrib_info = printElementAttributesAsString(inputElement);

                                    var Name = "tag:" + inputElement.tagName.toLowerCase();

                                    var text = inputElement.innerText




                                    if (isBlankString(text)) {
                                        text = "";
                                    } else {

                                        if (text.length <= 15) text = "@@text()=" + text;
                                        else text = "@@text()^" + text.slice(0,10);

                                    }


                                    var info = "按F8复制元素语法--> " + Name + attrib_info + text


                                    document.getElementById('show').textContent = info;
                                });
                            });
                        }


                        // 调用函数，添加事件
                        addClickEventToInputs();
'''
    set_nav_div='''
                    function createNavbar() {
                                    // 获取当前网页的标题
                                    const pageTitle = document.title;

                                    // 创建导航栏元素
                                    const navbar = document.createElement('div');
                                    navbar.classList.add('navbar2');
                                    navbar.id='daohanglan';


                                    // 设置导航栏样式
                                    navbar.style.backgroundColor = "lightgrey";  //浅灰色 
                                    navbar.style.color = 'green';
                                    navbar.style.fontSize = '25px';
                                    navbar.style.fontFamily = 'consolas';
                                    navbar.style.position = 'fixed'; // 将导航栏固定在页面顶部
                                    navbar.style.top = '0'; // 距离顶部的距离为 0
                                    
                                    navbar.style.width = '100%'; // 宽度占满整个屏幕
                                    navbar.style.zIndex = '5000'; // 确保导航栏在页面上方显示
                                    
                                    // 创建导航栏文本元素
                                    const navText = document.createElement('span');
                                    navText.textContent = '骚神库元素语法自动显示插件';
                                    navText.id = 'show';

                                    
                                    
                                    // 将文本添加到导航栏中
                                    navbar.appendChild(navText);

                                    // 将导航栏添加到页面的 body 元素中
                                    document.body.insertBefore(navbar, document.body.firstChild);
                                    // 创建复制按钮元素
                                //const copyDiv = document.createElement('div');
                                //copyDiv.textContent = '按F8 复制当前位置语法';
                                //copyDiv.style.float = 'left'; // 将按钮放置在最右边
                                //copyDiv.style.marginLeft = '10px'; // 设置右边距
                                //copyDiv.style.marginTop = '5px'; // 设置右边距
                                //copyDiv.style.fontSize = '18px';

                                // 将按钮添加到导航栏中
                                //navbar.appendChild(copyDiv);

                                }

                                // 调用函数创建导航栏
                                createNavbar();




                        '''
 
    listen_F8='''

                document.addEventListener('keydown', function(event) {
                                        // 检查是否按下了f8键（keyCode为18）
                                        if (event.keyCode === 119) {
                                            // 打印当前网页标题
                                            //console.log('Current page title: ' + document.title);
                                            tishi=document.getElementById('show').textContent;
                                            tishi2=tishi.substring(13)
                                            
                                            alert('已经复制该语法到剪贴板  '+tishi2);
                                        }
                                    });
                    '''
    listen_F8_2='''

                    document.addEventListener('keydown', function(event) {
                        // 检查是否按下了F8键（keyCode为119）
                        if (event.keyCode === 119) {
                            // 获取需要复制的文本内容
                            var tishi = document.getElementById('show').textContent;
                            var tishi2 = tishi.substring(13);

                            // 使用Clipboard API将tishi2的内容复制到剪贴板
                            navigator.clipboard.writeText(tishi2).then(function() {
                                // 复制成功后的操作
                                alert('已经复制该语法到剪贴板: ' + tishi2);
                            }).catch(function(error) {
                                // 复制失败处理
                                console.error('复制到剪贴板失败: ', error);
                                alert('复制失败');
                            });
                        }
                    });

                    '''