# https://localapi-doc-zh.adspower.net/docs/overview

import requests

序列号=10

json_data = {
    "name":str(序列号),
    "user_proxy_config": {
        "proxy_type": "http",
        "proxy_host": "123.0.0.1",
        "proxy_port": "12",
        "proxy_user": "12",
        "proxy_password": "12",
        "proxy_soft": "luminati",
    },
    "fingerprint_config": {
        "automatic_timezone": "1",
        "language": ["en-US", "en", "zh-CN", "zh"],
        "ua": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141",
        "flash": "block",
        "webrtc": "disabled",
    }
}

response = requests.post(
    "http://local.adspower.net:50325/api/v1/user/create", json=json_data
)

print(response.json())
